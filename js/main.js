$(document).ready(function () {
    //instagram feed
    var userFeed = new Instafeed({
        get: 'user',
        userId: '4861148196',
        limit: 12,
        resolution: 'standard_resolution',
        accessToken: '4861148196.1677ed0.e0d7e5cdad07499aa6d9391ba282f427',
        sortBy: 'most-recent',
        template: '<div class="col-lg-3 col-md-3 col-xs-6 instaimg"><a href="{{link}}" title="{{caption}}" target="_blank"><img src="{{image}}" alt="{{caption}}" class="img-fluid"/></a></div>',
      });
      userFeed.run();

    TweenLite.to($('.app-wrapper'), 0.6, {
        autoAlpha: 1,
        ease: Power1.easeOut,
    });

    //onlaod animation
    var tl = new TimelineLite();
    var anima = $(".anima");

    var tl2 = new TimelineLite();
    var anima2 = $(".anima2");

    tl.staggerFrom(anima, 1, {
        y: 20,
        autoAlpha: 0
    }, 0.2);

    tl2.staggerFrom(anima2, 1, {
        y: -20,
        autoAlpha: 0
    }, 0.2);


    $("section").each(function (index, elem) {
        var ctrl = new ScrollMagic.Controller({
            globalSceneOptions: {
                triggerHook: "onEnter"
            }
        });

        var $scrollAnima = $(elem).find('.scroll-a');

        var scrollTween = new TimelineMax();

        scrollTween.staggerFromTo($scrollAnima, 1, {
            y: 20,
            autoAlpha: 0,
        }, {
            y: 0,
            autoAlpha: 1,
        }, 0.2);

        var scene = new ScrollMagic.Scene({
                triggerElement: elem,
            })
            .setTween(scrollTween)
            .addTo(ctrl);
    });





    //banner animation
    /*   var slides = $('.slider-wrapper img');
      setInterval(function () {
          var current = slides.filter('.current'),
              next = current.next();

          if (!next.length) {
              next = slides.first();
          }

          slides.removeClass('off');
          current.removeClass('current').addClass('off');
          next.removeClass('off').addClass('current'); 
         
      }, 3000); */

    var pictxtnumber = 1;
    loadpictxt(pictxtnumber);

    var fadeintime = 500;
    animatediv();

    function animatediv() {
        var number = 0;
        var interval = setInterval(function () {
            pictxtnumber = pictxtnumber + 1;
            if (pictxtnumber > 4) {
                pictxtnumber = 1;
            }
            loadpictxt(pictxtnumber);
            $('#stopanim').on('click', function () {
                clearInterval(interval);
            });
        }, 3000);
    }

    function loadpictxt(num) {
        $('.slider-wrapper').html('');
        $(".slider-wrapper-hidden img:nth-child(" + num + ") ").clone().appendTo('.slider-wrapper');
        $('.slider-wrapper img').css('opacity', '0');
         $('.slider-wrapper img').animate({
            opacity: "1"
        }, 800);
       
    }

    //menu on scroll
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > 50) {
            $(".logo-white").hide();
            $(".logo-blue").show();
            TweenLite.to($(".menu-section"), 0.3, {
                backgroundColor: "#FFFFFF"
            });
            TweenLite.to($(".main-menu li a"), 0.3, {
                css: {
                    color: "#57CFEB"
                }
            });
        } else {
            $(".logo-white").show();
            $(".logo-blue").hide();
            TweenLite.to($(".menu-section"), 0.3, {
                backgroundColor: "transparent"
            });
            TweenLite.to($(".main-menu li a"), 0.3, {
                css: {
                    color: "#FFFFFF"
                }
            });
        }
    });

    

});